~~~~~~~~~~~~~~~~~~~~~~~~~~~ANDROID~~~~~~~~~~~~~~~~~~~~~~~~~~~~


CONTEXTE : La société Newspaper nous a contacté pour développer une application permettant de dématérialiser son journal papier.

OBJECTIF: Création de l'interface utilisateur.

HISTORIQUE : Il existe déjà une API appelée NewsAPI.


BESOIN : Pouvoir consulter les derniers articles du journal sur une application mobile.

CONTRAINTES : La récupération des données se fera via l’API Rest et la société apporte une importance particulière pour le design de son application.

DÉLAIS : La date limite de rendu du projet est le 31 Mai 2022.

FONCTIONNALITÉS :

* Pouvoir consulter les articles
* Pouvoir rechercher selon différents critères (Mots clés ou phrase, Catégories, Dates)
* Pouvoir ajouter un article en favori et consulter la liste
* Pouvoir modifier la liste des sources
* Pouvoir modifier la langue de l'application


FILTRES : Pays, Catégories, Sources, Mots clés ou phrase, Dates

COMPONENTS : WebView -> Permet d'afficher des pages web

